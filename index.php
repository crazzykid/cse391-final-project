<?PHP
include("login.php");
check_session();
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Login Page</title>
    <link rel='stylesheet' href='http://codepen.io/assets/libs/fullpage/jquery-ui.css'>
    <link rel="stylesheet" href="./css/style.css">
</head>
<body>

<div class="app-header-bar  centered">
    <div class="header content clearfix">
    </div>
</div>

<div class="login-card">
    <img src="./images/outlet.png" align="">

    <h1>Plugin</h1><br>

    <form method="post">
        <input type="text" name="user" placeholder="Username">
        <input type="password" name="pass" placeholder="Password">
        <input type="submit" name="login" class="login login-submit" value="login">
    </form>

    <div class="login-help">
        <a href="registration.php">Register</a> • <a href="#">Forgot Password</a>
    </div>
</div>

<script src='http://codepen.io/assets/libs/fullpage/jquery_and_jqueryui.js'></script>

</body>

</html>