# == CREATE TABLES HERE == #
#					       				 		#
#   					  					  #
#					  	  					 #
############################

DROP DATABASE IF EXISTS nimbusdb;

CREATE DATABASE nimbusdb;

USE nimbusdb;

CREATE TABLE `User` ( 
	`Email`		VARCHAR(100),
	`LastName`	CHAR(100) NOT NULL,
	`FirstName`	CHAR(100) NOT NULL,
	`Password`	CHAR(100) NOT NULL,
PRIMARY KEY (`Email`)	);

CREATE TABLE `File` (
`ID` INTEGER AUTO_INCREMENT,
`Owner`	   VARCHAR(100) NOT NULL,
`FileName` VARCHAR(100) NOT NULL,
`Directory` VARCHAR(200) NOT NULL,
`UploadDate` DATETIME,
`Type` VARCHAR(200),
`Size`  INTEGER(11),
`Trash` ENUM(  '0',  '1' ) NOT NULL DEFAULT  '0'
PRIMARY KEY (`ID`),
FOREIGN KEY (`Owner`) REFERENCES `User`(`Email`)
		ON DELETE NO ACTION
		ON UPDATE CASCADE);

CREATE TABLE `Shared` (
`FileID`		INTEGER,
`SharedWith`	VARCHAR(100) NOT NULL,
PRIMARY KEY (`FileID`,`SharedWith`),
FOREIGN KEY (`FileID`) REFERENCES `File` (`ID`)
		ON DELETE NO ACTION
		ON UPDATE CASCADE,
FOREIGN KEY (`SharedWith`) REFERENCES `User` (`Email`)
		ON DELETE NO ACTION
		ON UPDATE CASCADE	);
