<?php

	/**
	 * Singleton Database class
	 */
	class Database {

		private static $instance;

		private $connection;

		/**
		 * Applies query to the db
		 */
		public function take_query($query) {

			$error = $this->get_connect_error();

			$result = $this->connection->query($query);

			return $result;
		}

		/**
		 * Checks the MySQL connection and delivers an error message if the connection fails.
		 */
		public function check_connection() {

			$error = $this->get_connect_error();

			if ($error) {
				echo "check_connection() has failed. Could not connect to database.";
				trigger_error('Database connection failed: ' . $error, E_USER_ERROR);
			}
		}

		/**
		 * Takes a SQL query and converts it into a statement.
		 * @return statement
		 */
		public function prepare_statement($query) {
			
			$error = $this->get_connect_error();

			$statement = $this->connection->prepare($query);
			if($statement === false) {
				echo "prepare_statement() failed. Failed to recognize query.";
				trigger_error('Wrong SQL: ' . $query . ' Error: ' . $error, E_USER_ERROR);
			}

			return $statement;
		}

		private function get_connect_error() {

			$error = $this->connection->connect_error;
			return $error;
		}

		/** Get singleton */
		public static function get_instance() {

			if (!isset(Database::$instance)) {
				Database::$instance = new Database();
			}

			return Database::$instance;
		}

		// private constructor
		final private function __construct() {

			$user = "root";
			$password = "root";
			$db_name = "nimbusdb";
			$host = 'localhost';

			$this->connection = new mysqli($host, $user, $password, $db_name);

			$this->check_connection();

		}

		// We do not want clone to be implementable.
		final private function __clone() {}

	}

	/**
	 * Utilities class for Regular Expression replacement.
	 */
	class RegExUtilities {

		/**
		 * Replaces the underscores from a string with spaces.
		 */
		public static function replace_underscores($string) {

			return preg_replace("(_)", " ", $string);
		}

		/**
		 * Replaces the backticks with single quotes.
		 */
		public static function replace_backticks($string) {

			return preg_replace("(`)", "'", $string);
		}

		/**
		 * Replaces the single quotes with backtaicks.
		 */
		public static function replace_singlequotes($string) {

			return preg_replace("(')", "`", $string);
		}		

	}

	/**
	 * Manages the articles of the database.
	 */
	class DataManager {

		private static $instance;

		private $db;

		/**
		* This function updates the body of the string argument passed in
		*/
		public function exists($title) {

			// Make sure the title passed in has spaces instead of underscores
			$refined_title = RegExUtilities::replace_underscores($title);

			$exists = $this->check_title($refined_title);
			return $exists;
		}

		/**
		* Checks if title exists in DB
		*/
		private function check_title($title) {

			$query = "SELECT * FROM article WHERE title = '$title'";

			$result = $this->db->take_query($query);

			if ($result) {

				/* determine number of rows result set */
				$row_count = $result->num_rows;

				/* close result set */
				$result->close();
			}

			return $row_count;
		}

		/**
		 * Gets the body of matching the title of an article, if it exists.
		 */
		public function get_body($title) {

			// replace '_'s with spaces in the title
			$refined_title = RegExUtilities::replace_underscores($title);

			$body = $this->get_raw_content($refined_title);

			// Remove back ticks, which were included to serve as single quotes so the MySQL server would not complain.
			$body = RegExUtilities::replace_backticks($body); 

			return $body;
		}

		/**
		* Gets the body of an article from its corresponding title from the database.
		*/
		private function get_raw_content($title) {

			// Search for an article with this title.
			$query = "SELECT body FROM article WHERE title = '$title'";

			$statement = $this->db->prepare_statement($query);
			
			/* Execute statement */
			$statement->execute();
			
			// Assign the body of the article to $body here.
			$statement->bind_result($body);
			$statement->fetch();
			$statement->close();

			return $body;
		}
		
		public function query_db($query) {
			$result = $this->db->take_query($query);

			return $result;
		}


		/** 
		 * Insert a new article with a title and body into the database.
		 */
		public function add_new_article($title, $new_body) {

			$body = RegExUtilities::replace_singlequotes($new_body);

			$query = "INSERT INTO article (title, body) VALUES ('$title', '$body')";

			$result = $this->db->take_query($query);

			// Return a message saying whether or not this query succeeded.
			if ($result) {
				$status = "CREATED";
			} else {
				$status = "FAILED";
			}

			return $status;
		}

		/**
		* This function updates the body of the string argument passed in.
		* This function will create a new article if it does not exist.
		* 
		* Returns "UPDATED", "CREATED", OR "FAILED"
		*/
		public function set_content($title, $new_body) {

			// Replace single quotes with back quotes for SQL server.
			$body = RegExUtilities::replace_singlequotes($new_body);

			// Check to see that title is valid
			if (!$this->check_valid_title($title)) {
				// Signal that this function has failed.
				$status = "0";
				return $status;
			}

			if (!$this->exists($title))
				$status = $this->add_new_article($title, $body); // creating a new article
			else
				$status = $this->update_content($title, $body);

			return $status;
		}

		/**
		 * Check if the given title only contains valid characters.
		 */
		private function check_valid_title($title) {

			$regex = '/^[a-zA-Z0-9 :\-\(\)]+$/i';

			return preg_match($regex, $title);
		}

		/**
		 * Update an existing article and give it a new body.
		 */
		private function update_content($title, $new_body) {
			
			$query = "UPDATE article SET body='$new_body' WHERE title='$title'";
			
			$result = $this->db->take_query($query);

			// Return a message saying whether or not this query succeeded.	
			if($result) {
				$status = "UPDATED";
			}
			else {
				$status = "FAILED";	
			}

			return $status;
		}

		/** Get singleton */
		public static function get_instance() {

			if (!isset(DataManager::$instance)) {
				DataManager::$instance = new DataManager();
			}

			return DataManager::$instance;
		}

		// private constructor
		final private function __construct() {

			$this->db = Database::get_instance();

		}

		// We do not want clone to be implementable.
		final private function __clone() {}

	}
	
	?>
