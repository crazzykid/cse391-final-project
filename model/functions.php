<?PHP 
		include("connection.php");

		$file_names = array();

		function login($user, $pass){
				$db_manager = DataManager::get_instance();
				$userLogin = preg_replace('#[^A-Za-z0-9@.]#i', '', $user);
				$passwordLogin = preg_replace('#[^A-Za-z0-9@]#i', '', $pass);
				$salt = '$2a$10$1qAz2wSx3eDc4rFv5tGb5t';
				$hashpassword = crypt($passwordLogin, $salt);
				$query = "SELECT * FROM User WHERE email='$userLogin'
				 AND password='$hashpassword' LIMIT 1";
				$check_email = $db_manager->query_db($query);

				$lastName= "";
				$email = "";
				$firstName = "";

				 if($check_email) {
					 foreach ($check_email as $row){
						$firstName = $row["FirstName"];
						$lastName= $row["LastName"];
						$email = $row["Email"];
						
						$_SESSION["lastName"] = $lastName;
						$_SESSION["email"] = $email;
						$_SESSION["firstName"] = $firstName;
						$_SESSION["password_login"] = $hashpassword;
					 }
					
				 }
		}


		function reg($firstname,$lastname,$email, $password){
			$db_manager = DataManager::get_instance();
			$salt = '$2a$10$1qAz2wSx3eDc4rFv5tGb5t';
			$hashpassword = crypt($password, $salt);

			$check_email = "Select * FROM User WHERE email ='$email'";
			$check_email = $db_manager->query_db($check_email);
			$query = "INSERT INTO User VALUES ('$email', '$lastname', '$firstname', '$hashpassword');";
			return $db_manager->query_db($query);



		}

		function uploadFile($query){
			$db_manager = DataManager::get_instance();
			return $db_manager->query_db($query);
		}
		
			function queryDB($query){
			$db_manager = DataManager::get_instance();
			return $db_manager->query_db($query);
		}

		function getFiles($email, $sortBy, $asc = true, $fromTrash = false){
            $flag = $fromTrash ? 1 : 0;
            $order = $asc ? "ASC" : "DESC";
            
            // $sortBy is either FileName, UploadDate, or Size

			$db_manager = DataManager::get_instance();
			$query = "SELECT * FROM File WHERE Owner='$email' AND Trash='$flag' ORDER BY $sortBy $order " ;
			return $db_manager->query_db($query);
		}


        function getFilesFilter($email, $type, $sortBy, $asc = true, $fromTrash = false){
            $flag = $fromTrash ? 1 : 0;
            $order = $asc ? "ASC" : "DESC";

            // $sortBy is either FileName, UploadDate, or Size

            $db_manager = DataManager::get_instance();
            $query = "SELECT * FROM File WHERE Owner='$email' AND Type='$type' AND Trash='$flag' ORDER BY $sortBy $order " ;
            return $db_manager->query_db($query);


        }


		function check_session() {
			// If user is already logged in, header the user to the home page
			if (!isset($_SESSION["email"])) {
				echo"";
			}
			else{
				echo "<meta http-equiv=\"refresh\" content=\"0; url=home/account.php\">";
			}
		}
		

		//query the db and display all the content of the user 
		function showContent($result){
			
                        foreach($result as $row ){
							$file_type = "unknown";
                            $id = $row['ID'];
                            $owner = $row['Owner'];
                            $date_added = $row['UploadDate'];
                            $fileName = $row['FileName'];
                            $directory = $row['Directory'];
                            $type = $row["Type"];
                            $size = $row['Size'];
							$html_type = $type;
                            echo '<tr>';
                            echo '<div class="large-10 columns">';
                            if($directory != "" && $fileName != "") {
                                echo "<td>";
								
                                if(strcmp($html_type,"image")==0){
									echo "<input type='checkbox' name='file[]' id='file' value='$directory/$fileName'>";
                                    echo "<img class='img-thumbnail' width='70' height='60' onclick='insertElement(this.src, \"img\", \"\")' data-toggle='modal'
									 data-target='#displayModal' src='Documents/$directory/$fileName'/> ";
                                    echo $fileName. "<br><br>";
                                } 
								
								else if(strcmp($html_type,"video")==0){
									echo "<input type='checkbox' name='file[]' id='file' value='$directory/$fileName'>";
                                    echo "<video class='img-thumbnail' width='70' height='60'  src='Documents/$directory/$fileName'
									 onclick='insertElement(this.src, \"video\", \"\")' data-toggle='modal'
									 data-target='#displayModal'>
                                  <source src='Documents/$directory/$fileName' type='video/mp4'>
                                  </video>";
								   echo $fileName. "<br><br>";
                                } 
								
								// TODO
								else if(strcmp($html_type,"text")==0){
								    echo "<input type='checkbox' name='file[]' id='file' value='$directory/$fileName'>";
                                    echo "<img class='img-thumbnail' width='70' height='60' alt='Documents/$directory/$fileName' 
									onclick='insertElement(this.alt, \"iframe\",\"text\")' data-toggle='modal'
                                     data-target='#displayModal' src='../images/txt_img.png'/> ";
                                    echo $fileName. "<br><br>";
                                }
								
								else if (strcmp($html_type, "audio") == 0) {
								    echo "<input type='checkbox' name='file[]' id='file' value='$directory/$fileName'>";
                                    echo "<img class='img-thumbnail' width='70' height='60' alt='Documents/$directory/$fileName' 
									onclick='insertElement(this.alt, \"audio\",\"\")' data-toggle='modal'
                                     data-target='#displayModal' src='../images/audio_img.png'/> ";
                                    echo $fileName. "<br><br>";	
								}
								
								// pdfs, documents, zip
								else if (strcmp($html_type, "document") == 0) {
								    echo "<input type='checkbox' name='file[]' id='file' value='$directory/$fileName'>";
                                    echo "<img class='img-thumbnail' width='70' height='60' alt='Documents/$directory/$fileName' 
									 onclick='insertElement(this.alt, \"iframe\",\"text\" )' data-toggle='modal'
                                     data-target='#displayModal' src='../images/doc_img.png'/> ";
                                    echo $fileName. "<br><br>"; 	
								}
								
								else if (strcmp($html_type, "archive") == 0) {
								    echo "<input type='checkbox' name='file[]' id='file' value='$directory/$fileName'>";
                                    echo "<img class='img-thumbnail' width='70' height='60' src='../images/archive_img.png'/> ";
                                    echo $fileName. "<br><br>"; 	
								}
								
								else if (strcmp($html_type, "binary") == 0) {
								    echo "<input type='checkbox' name='file[]' id='file' value='$directory/$fileName'>";
                                    echo "<img class='img-thumbnail' width='70' height='60' src='../images/exe_img.png'/> ";
                                    echo $fileName. "<br><br>"; 	
								}
								
								else {
									// unknown	
                                    echo "<input type='checkbox' name='file[]' id='file' value='$directory/$fileName'>";
                                    echo "<img class='img-thumbnail' width='70' height='60' src='../images/unknown_img.png'/> ";
                                    echo $fileName. "<br><br>"; 
								}
								
                                echo "</td>";
                                echo "<td>";
                                echo $date_added;
                                echo "</td>";

                                echo "<td>";
                                if ($size < pow(2,10)) {
                                    echo $size . " bytes";
                                } else if ($size < pow(2,20)) {
                                    echo round($size / pow(2,10), 2) . " KB";
                                } else if ($size < pow(2,30)) {
                                    echo round($size / pow(2,20), 2) . " MB";
                                } else if ($size < pow(2,40)) {
                                    echo round($size / pow(2,30), 2) . " GB";
                                }
                            
                                echo "</td>";
                            }
                            echo '</div>';
                            echo '</tr>';
                        }	
			
			
		}
		//Delete all the files in current directory
		function deleteDirectory($dir) {
						if (!file_exists($dir)) {
							return true;
						}
					
						if (!is_dir($dir)) {
							return unlink($dir);
						}
					
						foreach (scandir($dir) as $item) {
							if ($item == '.' || $item == '..') {
								continue;
							}
					
							if (!deleteDirectory($dir . DIRECTORY_SEPARATOR . $item)) {
								return false;
							}
					
						}
					
						return rmdir($dir);
					}
?>
