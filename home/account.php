<?PHP
        include("../model/functions.php");
        session_start();
        include("homefunction.php");
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Nimbus</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        
        <!--/.nav-collapse -->
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="account.php">Nimbus</a>
        </div>
        <script> 
        		 
        
        
        </script>

        <!-- Top Menu Items -->
        <ul class="nav navbar-right top-nav">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i
                        class="fa fa-user"></i> <?PHP echo $user; ?> <b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                    </li>
                </ul>
            </li>
        </ul>
        <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav side-nav">
                <li class="active">
                    <a href="account.php"><i class="fa fa-fw fa-cloud-upload"></i> Files</a>
                </li>

                <li>
                    <a href="trash.php"><i class="fa fa-fw fa-trash"></i> Trash</a>                                                                                                                 
                </li>


                
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </nav>

    <div id="page-wrapper">
               

        <div class="container-fluid">   
            
                                                                                                                                                                             <br>    
                <div class="btn-toolbar">

                    <form method="GET" action=""> 

                        <div class="col-xs-2 selectContainer">
                            <select class="form-control" name='filter'>
                        <option value="all" <?php if ($_GET['filter'] == 'all' || empty($_GET['filter'])) echo "selected" ?> >All</option>
                        <option value="image" <?php if ($_GET['filter'] == 'image') echo "selected" ?> >Image</option>
                        <option value="video" <?php if ($_GET['filter'] == 'video') echo "selected" ?> >Video</option>
                        <option value="audio" <?php if ($_GET['filter'] == 'audio') echo "selected" ?> >Audio</option>
                        <option value="document" <?php if ($_GET['filter'] == 'document') echo "selected" ?> >Document</option>
                        <option value="text" <?php if ($_GET['filter'] == 'text') echo "selected" ?> >Text</option>
                        <option value="data" <?php if ($_GET['filter'] == 'data') echo "selected" ?> >Data</option>
                        <option value="archive" <?php if ($_GET['filter'] == 'archive') echo "selected" ?> >Archive</option>
                        <option value="binary" <?php if ($_GET['filter'] == 'binary') echo "selected" ?> >Binary</option>
                        <option value="unknown" <?php if ($_GET['filter'] == 'unknown') echo "selected" ?> >Unknown</option>
                            </select>
                        </div>
                        
                        <input class="btn btn-primary btn-md"  type="submit" value="Filter">


                        <input class="btn pull-right btn-default btn-lg" style="margin-right:10px" class="btn btn-red" type="submit"  id="delete" 
                        name="delete" value="Remove" form="controller">
                        <input class="btn pull-right btn-success btn-lg" style="margin-right:10px" type="submit"  id="download" name="download" 
                        value="Download" form="controller">
                        <!-- Button trigger modal -->
                        <button type="button" class="btn pull-right btn-primary btn-lg" style="margin-right:10px" data-toggle="modal" 
                        data-target="#uploadModal">
                        Upload
                        </button>
                    </form>
                </div>
      

<style>
.buttonlink {
     background:none!important;
     border:none; 
     padding:0!important;
     font: inherit;
     /*border is optional*/
      
     cursor: pointer;
}
</style>               


			<hr>

            <br>
            <table class="" style="width:100%">
                <tr>
                    <form method="GET" action="">
                    <th><button class="buttonlink"> Name &#8681; </button> <hr></th>
                    <th><button class="buttonlink">Modified</button><hr></th>
                    <th><button class="buttonlink">Size</button><hr></th>
                    </form>
                </tr>

                <form method="GET" id="controller" action="file_manager.php">
                <?PHP

                // TODO:
                $sortBy = "FileName";
                $desc = true;

				// getting all the files the user have and displaying them in there document
                    if ($_GET['filter'] == 'all' || empty($_GET['filter']))
                        $result = getFiles($email, $sortBy, $desc);
                    else
                    {
                        $type = $_GET['filter'];
                        $result = getFilesFilter($email, $type, $sortBy, $desc);
                    }

                    if($result){
						showContent($result);
                    }
                ?>
                </form>
                
            </table>

        </div>


    </div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->


<!-- Modal -->
<div class="modal fade" id="uploadModal" tabindex="-1" role="dialog" aria-labelledby="uploadModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="uploadModalLabel">Upload Content</h4>
            </div>
            <div class="modal-footer">
                <form action="" method="POST" enctype="multipart/form-data">
                    <input type="file" name="fileUpload">
                    <input class="btn btn-danger" type="submit" name="uploadpic" value="Upload">
                </form>
            </div>

        </div>
    </div>
</div>

<style>

.modal-backdrop.in {
    opacity: .9;
}

	.modal fade {
	  background-color: #0F0;
	  opacity: 1.0;
	}
	.modal-dialog {
	  background-color: transparent;
	}
	.modal-content {
	  background-color: #fff;
	}
	.modal-footer {
	  background-color: #fff;
	}
	

</style>

<div class="modal fade" id="displayModal" tabindex="-1" role="dialog" aria-labelledby="displayModalLabel">
    <div class="modal-dialog" role="document" >
        <div class="modal-content">
            
            <div class="modal-footer">
            
            	<!--DYNAMICALLY PUT IMAGE HERE-->
                <div id="image-frame" align="center">
                	<!--IMAGE WILL GO HERE-->
                </div>
                
            </div>

        </div>
    </div>
</div>

<script>
	function insertElement(path, type,fileType) {
		//$( "#image-frame" ).replaceWith( "<img></img>"
		//alert("this is type: " + type);
		if(fileType=="text"){
			$('#image-frame').empty();
			$('#image-frame').prepend($('<'+type+'>',{src: path, height: 686, width: 576, controls: ""}));
			
		} else {
			$('#image-frame').empty();
			$('#image-frame').prepend($('<'+type+'>',{src: path, width: 575, controls: ""}));
		}
	}
</script>

<!-- jQuery -->
<script src="js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="js/bootstrap.min.js"></script>

<!-- Morris Charts JavaScript -->
<script src="js/plugins/morris/raphael.min.js"></script>
<script src="js/plugins/morris/morris.min.js"></script>
<script src="js/plugins/morris/morris-data.js"></script>

</body>

</html>
