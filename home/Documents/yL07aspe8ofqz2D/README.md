- NBA Stat Comparer - 2015
	- previously (version 1.0 in Java), Game object was constructed using a field for all stats. However, since we only compare one stat at a time among players, use the builder pattern to make Game objects with only the parameters needed (Date and one stat).
	- The site I used: basketball monster, which is a great resource, became a pay-to-use app. So I decided to make my own version that was more suited to my needs and more friendly to the eye (images, tags(injuries, breakout, better ui).
	- Took my site to the web (version 2.0) with usernames.
	- Applied concepts in:
		- UI
		- MVC framework
		- Distributed systems
		- internet programming
		- probability and machine learning, guessing future stats, injuries, who will play more minutes, etc.
		- NoSQL (mongodb)
		- web mining. updates on players


Learn that when one player doesn't play, which player's minutes/productivity go up.
ex: when Russell Westbrook doesn't play, Kevin Durant's Points and Assists go up.
- Graph structure of all players


PHP Script that gets parses NBA stat data of the day from ESPN and posts the top productive players.


ESPN's old top productive players thing


shows production graph of date range.



can choose any stat, any two players:


every morning at 5:00AM, calculate the stats for all players of the day.
Create a sorted list for every category (per game and total).



player tags: 
- injury prone if they are injured more than 25% of their games.
- high scorer if they score more than 25 PPG
- great passer if their AST/TO is > 2.00
- assist machine if their APG 


merits

Top 5 Value


Top PPG
Top 5 PPG
Top 10 PPG	
Top 5 APG
Top 5 MPG
Top 5 RPG
Bottom 5 TPG
Top 5 RPG
Top 5 FFA PER GAME
Top 5 FFM PER GAME
BLOCKS
STEALS
3 POINTERS
Top 5 FG PER GAME

fouls




Top 5 Total Games

Top 5 TOTAL MINUTES

Top 5 FREE THROWS MISSED 


Top 5 TOTAL POINTS (GREEN)
Top 10 TOTAL POINTS (LIGHT GREEN)
Top 20 TOTAL POINTS (LIGHTER GREEN)

Top 10 AST/TO

dermits
BOTTOM 5 TOTAL POINTS (RED)
BOTTOM 10 TOTAL POINTS (LIGHT RED)
BOTTOM 20 TOTAL POITNS (LIGHTER RED)