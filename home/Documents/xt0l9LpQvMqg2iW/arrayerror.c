/**
 * Test to see array bounds error.
 */
#include <stdio.h>

 int main()
 {
 	int num[12];
 	int index, sum = 0;

 	for(index = 0; index < 12; index++)
 	{
 		num[index] = index;
 		printf("num[index] = %d\n", num[index]);
 	}

 	num[index] = index;
 	printf("num[index] = %d\n", num[index]);

 	for(index = 0; index < 12; index = index + 1)
 	{
 		sum += num[index];
 		printf("sum = %d\n", sum);
 	}

 	return 0;
 }