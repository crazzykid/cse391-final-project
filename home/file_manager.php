<?php
	 include("../model/functions.php");
	 session_start();
	 if (isset($_SESSION["email"])) {
            $firstName = $_SESSION["firstName"];
            $lastName = $_SESSION["lastName"];
            $user = $firstName . " " . $lastName;
            $email = $_SESSION["email"];
        } else {
            //echo "<meta http-equiv=\"refresh\" content=\"0; url=../index.php\">"; // replace with AJAx call
        }
	$download = $_GET['download'];
	$delete = $_GET['delete'];
    $restore = $_GET['restore'];
	$path = "Documents/";
	
	//checking to see if the user is trying to download a file
	if($download){
		$name = $_GET['file'];
		if(sizeof($name)>1){
			$zipname = 'attachments.zip';
			$zip = new ZipArchive;
			$zip->open($zipname, ZipArchive::CREATE);
			foreach ($name as $file) {
				$zipfile = basename($file);
			  	$zip->addFromString(basename($file),  file_get_contents($path.$file));  ;
			}
			$zip->close(); 
			 
			header('Content-Type: application/zip');
			header('Content-disposition: attachment; filename='.$zipname);
			header('Content-Length: ' . filesize($zipname));
			ob_clean();
			flush();
			readfile($zipname);
			unlink($zipname); // Now delete the temp file  
			exit;   
				
		}else 
			foreach($name as $file){
				if(file_exists($path.$file)) {
				header('Content-Description: File Transfer');
				header('Content-Type: application/octet-stream');
				header('Content-Disposition: attachment; filename="'.basename($file).'"');
				header('Expires: 0');
				header('Cache-Control: must-revalidate');
				header('Pragma: public');
				header('Content-Length: ' . filesize($path.$file));
				ob_clean();
				flush();
				readfile($path.$file);
				 exit;
			}
			 
			}//checking to see if the user is trying to delete a file
	}else if ($delete){
		
		$name = $_GET['file'];
		foreach ($name as $file) {
			$pieces = explode("/", $file);
			$directory = $pieces[0];
			$fileName = $pieces[1];
            $sql = "SELECT * FROM File WHERE Owner='$email' AND Directory='$directory' AND FileName='$fileName' AND Trash='1'";
		    $result = queryDB($sql);
            $num_rows = mysqli_num_rows($result);

            if ($num_rows > 0) {
	            // Called by trash
                $sql = "DELETE FROM File WHERE Owner='$email' AND Directory='$directory' AND FileName='$fileName' ";
			    $result = queryDB($sql);

                deleteDirectory($path.$directory);	
                
                header("Location: ./trash.php");
                #echo "<meta http-equiv=\"refresh\" content=\"0; url=./trash.php\">";
            } else {
                $sql = "UPDATE File SET Trash='1' WHERE Owner='$email' AND Directory='$directory' AND FileName='$fileName' AND Trash='0'";
                $result = queryDB($sql);
            }
		}
		
	} else if ($restore) {

        $name = $_GET['file'];
        foreach ($name as $file) {
            $pieces = explode("/", $file);
            $directory = $pieces[0];
            $fileName = $pieces[1];

            $sql = "UPDATE File SET Trash='0' WHERE Owner='$email' AND Directory='$directory' AND FileName='$fileName' AND Trash='1'";
            $result = queryDB($sql);

            header("Location: ./trash.php");
        }

    }


    echo "<meta http-equiv=\"refresh\" content=\"0; url=./account.php\">";


						
?>
