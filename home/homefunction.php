<?PHP

	function getFileTypeFromExtension($fileExtension) {
	
		$fileExtension = strtolower($fileExtension);
	
		if ($fileExtension == "")
			return "unknown";
			
		// Image
		$type = "image";
		switch ($fileExtension) {
		
			case "jpg":
			case "jpeg":
			case "png":
			case "gif":
			case "tif":
			case "tiff":
			case "jif":
			case "jfif":
				return $type;
				
		}
		
		// Document
		$type = "document";
		switch ($fileExtension) {
		
			case "pdf":
			case "doc":
			case "docx":
				return $type;
				
		}
		
		// Archive
		$type = "archive";
		switch ($fileExtension) {
		
			case "zip":
			case "rar":
			case "tar":
			case "7z":
			
				return $type;
				
		}
		
		// Text
		$type = "text";
		switch ($fileExtension) {
		
			case "txt":
			case "asp":
			case "aspx":
			case "css":
			case "htm":
			case "jsp":
			case "hxtml":
			case "xml":
			case "log":
			case "tex":
			case "py":
			case "textile":
			case "ruby":
			case "c":
			case "cpp":
			case "php":
			case "html":
			case "js":
			case "java":
			case "word":
				return $type;
				
		}
	
		// Data
		$type = "data";
		switch ($fileExtension) {
		
			case "csv":
			case "dat":
			case "ppt":
			case "pptx":
				return $type;
				
		}
		
		// Binary
		$type = "binary";
		switch ($fileExtension) {
		
			case "out":
			case "exe":
			case "bat":
			case "jar":
				return $type;
				
		}
		
		// Video
		$type = "video";
		switch ($fileExtension) {
		
			case "mpeg":
			case "mp4":
			case "avi":
			case "mpg":
			case "flv":
            case "mkv":
				return $type;
				
		}
		
		// Audio
		$type = "audio";
		switch ($fileExtension) {
		
			case "mp3":
			case "wav":
			case "wv":
			case "wma":
			case "ra":
			case "mid":
			case "ogg":
				return $type;
				
		}
		
		// Unknown
		return "unknown";	
		
	}



 // Check if user has logged in and getting user information from sesstion object
        if (isset($_SESSION["email"])) {
            $firstName = $_SESSION["firstName"];
            $lastName = $_SESSION["lastName"];
            $user = $firstName . " " . $lastName;
            $email = $_SESSION["email"];
            echo $firstName;
        } else {
            echo "<meta http-equiv=\"refresh\" content=\"0; url=../index.php\">"; // replace with AJAx call
        }

        $maxFile = 3221225472;
        //Post Image upload script
        if (isset($_FILES['fileUpload'])) {
            //$fileType = $_FILES["fileUpload"]["type"];
			$fileExtension = pathinfo($_FILES['fileUpload']['name'], PATHINFO_EXTENSION);
			
			$fileType = getFileTypeFromExtension($fileExtension);
			
            $size = $_FILES['fileUpload']['size'];

			
            $dateAdded = date("Y-m-d H:i:s");

            //only allow the user to upload at max 3 Gigabyte
            if(@$_FILES["fileUpload"]["size"] < $maxFile){
                $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                $rand_dir_name = substr(str_shuffle($chars), 0, 15);
                mkdir("Documents/$rand_dir_name");

                if (file_exists("Documents/$rand_dir_name/".@$_FILES["fileUpload"]["name"])){
                    echo @$_FILES["fileUpload"]["name"]." Already exists";
                }
                else{
                    move_uploaded_file(@$_FILES["fileUpload"]["tmp_name"],"Documents/$rand_dir_name/".$_FILES["fileUpload"]["name"]);
                    $fileName = @$_FILES["fileUpload"]["name"];

                    $sqlCommand = "INSERT INTO File VALUES('', '$email','$fileName','$rand_dir_name', '$dateAdded', '$fileType', '$size', '0')";
                    uploadFile($sqlCommand);
					echo "<meta http-equiv=\"refresh\" content=\"0; url=./account.php\">";
                }
            }
            else {
                echo "Invailid File! Your file must be less than 3g";
            }
        }
?>
