<?PHP 
	include("./model/functions.php"); ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Registration Page</title>
    <link rel="stylesheet" href="css/reg_style.css">
    <script type="text/javascript" src="./js/jquery.js"></script>
    <script type='text/javascript' src='./js/jquery.tipsy.js'></script>    
    <link rel="stylesheet" href="./css/tipsy.css" type="text/css"/>
</head>
<body>
<?PHP
//VARIABLES TO USE
$firstname = "";
$lastname = "";
$email = "";
$password = "";
$register = "";

$register = $_POST['register'];
$firstname = strip_tags($_POST['fname']);
$lastname = strip_tags($_POST['lname']);
$email = strip_tags($_POST['email']);
$password = strip_tags($_POST['password']);

if($register)
{
	
	$resultcount = reg($firstname,$lastname,$email, $password);
	
	echo "<meta http-equiv=\"refresh\" content=\"0; url=index.php\">";
	
}
?>

<div class="register-card">
    <h1>Sign Up</h1><br>

  <form action="#" method="post">
        <div id='forminputs'>
            <input type="text" rel="tipsy" id="fname" title="This field is required. Please enter your first name"
                   name="fname" placeholder="First Name" onblur=verifyFirstName()>
            <input type="text" rel = "tipsy" id="lname" title="This field is required. Please enter your last name"
                   name="lname" placeholder="Last Name" onblur=verifyLastName()>
            <input type="email" rel = "tipsy" id="email" title="This field is required. Please enter a valid email address"
                   name="email" placeholder="Email" onblur=verifyEmail()>
            <input type="password" rel = "tipsy" id="password" title="This field is required. Please enter a password."
                   name="password" placeholder="Password" onblur=verifyPassword()>
            <input type="password" rel = "tipsy" id="passconf" title="The passwords you entered do not match."
                   name="passconf" placeholder="Confirm Password" onblur=verifyConfirmPassword()>
            <br><br>
        </div>

        <input type="submit" name="register" class="register register-submit" value="Submit">
    </form>
</div>
</body>

<script type='text/javascript' src='./js/error_bubble.js'></script>

</html>