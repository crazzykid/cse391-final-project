    $(function() {
        $('input[rel=tipsy]').tipsy({gravity: 'w', trigger: 'manual'});
        $('div[rel=tipsy]').tipsy({gravity: 'w', trigger: 'manual'});
        $('select[rel=tipsy]').tipsy({gravity: 'w', trigger: 'manual'});
    });

    var firstName = document.getElementById("fname");
    var lastName = document.getElementById("lname");
    var email = document.getElementById("email");
    var password = document.getElementById("password");
    var confirmPassword = document.getElementById("passconf");
    var male = document.getElementById("M");
    var female = document.getElementById("F");
    var schoolSelect = document.getElementById("schoolselect");
    var re = new RegExp(/(.+)@(.+){2,}\.(.+){2,}/);

    function verifyFirstName() {
        if (firstName.value == "") {
            $("#fname").tipsy("show");
        }
        else
            $("#fname").tipsy("hide");
    }
    function verifyLastName() {
        if (lastName.value == "")
            $("#lname").tipsy("show");

        else
            $("#lname").tipsy("hide");
    }
    function verifyEmail() {
        if (re.test(email.value) == false) {
            $("#email").tipsy("show");
        }
        else {
            $("#email").tipsy("hide");
        }
    }
    function verifyPassword() {
        if (password.value == "")
            $("#password").tipsy("show");
        else {
            $("#password").tipsy("hide");
        }
    }
    function verifyConfirmPassword() {
        if ((!password.value.match(confirmPassword.value)) || confirmPassword.value == "") {
            $("#passconf").tipsy("show");
        }
        else
            $("#passconf").tipsy("hide");
    }
    function verifyGender(){

        if((!male.checked) && (!female.checked))
            $("#genders").tipsy("show");
        else
            $("#genders").tipsy("hide");
    }
    function verifySchool(){
        if(schoolSelect.value=="None")
            $("#schoolselect").tipsy("show");
        else
            $("#schoolselect").tipsy("hide");
    }